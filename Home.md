Welcome to Kaidan's wiki! Here you can find useful information about Kaidan for users and developers.

## Setup and Collaboration for Contributing to Kaidan

There is a guide for a [basic setup](setup) targeted at contributors who are unfamiliar with KDE Identity, GitLab, Git or QtCreator and want to start contributing quickly.
If you work on a branch together with other people, the [collaboration section](git/collaboration) can improve your workflow and avoid problems.

## Building Kaidan from Sources

There are multiple guides describing how to build Kaidan from sources:

* [Building Kaidan on Debian-based systems](building/linux-debian-based)

## Using Kaidan without Building It

There are multiple ways of using Kaidan:

* [Installing Kaidan via Flatpak on Linux](using/flatpak)

## Optimizing Graphics

If you would like to contribute graphics to Kaidan, there is a [guide for optimizing graphics](optimizing-graphics).

## FAQs

* [FAQ for Users](faqs/users)
* [FAQ for Developers](faqs/developers)
