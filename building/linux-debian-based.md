# Setting Up Kaidan on Debian-based Systems

This page describes how to manually set up Kaidan on Debian-based systems (i.e. Ubuntu, KDE Neon, Linux Mint, etc.).
It is also meant as a description for setting up Kaidan on other Linux distributions.

In this guide, **replace `$HOME` with the path of the directory that will contain all development data**.

## Non Debian-based Systems

If you are running a distribution that is not based on debian, you can still follow this guide, by running debian in [distrobox](https://distrobox.privatedns.org/). Of course Kaidan also compiles on other distributions, but we do not want to collect the package names for every distribution out there here.

```bash
distrobox create --image docker.io/debian:testing
```

## Install *Dependencies*

Make sure that your distribution provides at least the minimum versions of [Kaidan's dependencies](https://invent.kde.org/network/kaidan#dependencies).

```bash
sudo apt install git cmake extra-cmake-modules ninja-build build-essential \
  qt6-base-dev qt6-declarative-dev qt6-declarative-private-dev \
  qt6-svg-dev qt6-multimedia-dev qt6-positioning-dev qt6-location-dev \
  qml6-module-qtquick-effects qml6-module-qtpositioning qml6-module-qtlocation \
  libkf6crash-dev libkf6kio-dev kio-extras kquickimageeditor-dev \
  libkirigami-dev kirigami-addons-dev libkf6notifications-dev libkdsingleapplication-qt6-dev gstreamer1.0-plugins-bad \
  libkf6prison-dev libkf6prisonscanner6 qml6-module-org-kde-prison \
  ffmpegthumbs breeze-icon-theme fonts-noto-color-emoji
```

## Set Up *Desktop Style* for Desktop Systems without a Qt Integration (Optional)

If your system has no Qt integration (e.g., on systems with GNOME such as the regular Ubuntu), you can set up the correct desktop style.
For KDE Plasma, you do not need that since it has such an integration.

```bash
sudo apt install libkf6qqc2desktopstyle-data qml-module-org-kde-qqc2desktopstyle
```

The following example sets `org.kde.desktop`, which is already the default, as the desktop style that will be used by Kaidan.

```bash
echo "export QT_QUICK_CONTROLS_STYLE=org.kde.desktop" >> ~/.profile
source ~/.profile
```

## Set Up Theming for Desktop Environments other than KDE Plasma (e.g., GNOME)

To get a more appropriate look of Kaidan, you can use the configuration tool `qt6ct`.

```bash
sudo apt install qt6ct adwaita-qt6
qt6ct
export QT_QPA_PLATFORMTHEME=qt6ct
echo "export QT_QPA_PLATFORMTHEME=qt6ct" >> ~/.profile
source ~/.profile
```

After opening the configuration tools, set the settings you want for each of them.

For example with GNOME on Ubuntu, you might want to set the widget theme to *Adwaita*, the standard dialogs to *GTK3* (for the file dialog) and the icon theme to *Breeze*.
Furthermore, you can adjust the font to *Ubuntu Regular* with size *11* and *Ubuntu Mono Regular* with size *13*.

An alternative is to use a platform theme.

For GNOME, you can install and configure the corresponding platform theme accordingly:

```bash
sudo apt install qgnomeplatform-qt6
export QT_QPA_PLATFORMTHEME=gnome
echo "export QT_QPA_PLATFORMTHEME=gnome" >> ~/.profile
source ~/.profile
```

## Set Up OMEMO Library *libomemo-c* (Optional)

libomemo-c can either be used via a Debian package or by building it manually.
If you need a more recent version of libomemo-c than the one provided by your distribution, you must build and install it manually.
As of 2023-04-04, there is an up-to-date package in Debian unstable/testing.

### Set Up library *protobuf-c* needed by libomemo-c

```bash
sudo apt install autoconf libtool
cd $HOME
git clone https://github.com/protobuf-c/protobuf-c.git
./autogen.sh
./configure --disable-protoc
make -j$(nproc)
sudo make install
```

### Set Up *libomemo-c*

```bash
cd $HOME
git clone https://github.com/dino/libomemo-c.git
mkdir libomemo-c/build && cd libomemo-c/build
cmake -G Ninja -DBUILD_SHARED_LIBS=ON ..
ninja
sudo ninja install
```

## Set Up XMPP Library *QXmpp*

QXmpp can either be used via a Debian package or by building it manually.
If you need a more recent version of QXmpp than the one provided by your distribution, you must build and install it manually.
As of 2024-12-26, there is no up-to-date package in Debian unstable/testing.

If your distribution provides an up-to-date package, you can install that:

```bash
sudo apt install libqxmpp-dev
```

Otherwise, you need to build it yourself.

Install the dependencies first:

```bash
sudo apt install libqca-qt6-dev libomemo-c-dev
```

Now you can build and install QXmpp:

```bash
cd $HOME
git clone https://github.com/qxmpp-project/qxmpp.git
mkdir qxmpp/build && cd qxmpp/build
cmake -G Ninja -DQT_VERSION_MAJOR=6 -DBUILD_OMEMO=ON -DBUILD_TESTS=OFF -DBUILD_EXAMPLES=OFF ..
ninja
sudo ninja install
```

In order to generate QXmpp's documentation, you need to install Doxygen via `sudo apt install doxygen` and add `-DBUILD_DOCUMENTATION=ON` to the `cmake` command.
For building QXmpp with A/V call support, you need to install GStreamer via `sudo apt install libgstreamer1.0-dev` and add `-DWITH_GSTREAMER` to the `cmake` command.

## Set Up *Kaidan*

### Get the Source Code

```bash
cd $HOME
git clone https://invent.kde.org/network/kaidan.git
```

### Build

```bash
mkdir kaidan/build && cd kaidan/build
cmake -G Ninja ..
ninja
```

### Run

```bash
bin/kaidan
```

If you get the error *kaidan: error while loading shared libraries: libomemo-c.so.0: cannot open shared object file: No such file or directory*, update the cache before running Kaidan again.

```bash
sudo ldconfig
```

### Install (Optional)

```bash
sudo ninja install
```

Once you installed Kaidan, you can run it from anywhere.

```bash
kaidan
```

### Uninstall (Optional)

Remove compiled files.

```bash
sudo ninja uninstall
```

Remove remaining data such as graphics or JSON files.

```
sudo rm -r /usr/share/kaidan
```

Remove configuration and database files.

```bash
sudo rm -r ~/.config/kaidan ~/.local/share/kaidan
```
