# Frequently Asked Questions by Developers

This page should help developers to find solutions for known problems.

## Kaidan is not built. What should I do?

### The reason is unclear.

Remove the *build* directories of all applications built by yourself (like Kaidan itself, QXmpp, Kirigami etc.) and build them again.

### The reason seems to be the linking with QXmpp.

Remove all QXmpp related files which were installed on your system by `make install`:

```bash
sudo rm -r $(find /usr/local -name "*qxmpp*" -or -name "*QXmpp*")
```

After that you may install QXmpp again via `make install`.

## How can I uninstall software that does not provide `make uninstall`?

You can manually remove all files listed in `build/install_manifest.txt` and their directories if they are empty after deletion of the containing files:

```bash
cat install_manifest.txt | xargs sudo rm && cat install_manifest.txt | xargs -L1 dirname | xargs sudo rmdir -p --ignore-fail-on-non-empty 2> /dev/null
```

## How can I focus a QML item appropriately?

Especially text fields should often be focused in order to achieve a more fluent work flow.
You can use `forceActiveFocus()` for that.
It sets `activeFocus` to `true`.
But another item might be focused after you called `forceActiveFocus()`.
You can determine which item is focused:

```
Connections {
    target: applicationWindow()

    function onActiveFocusItemChanged() {
        print(applicationWindow().activeFocusItem)
    }
}
```

Afterwards, you are able to force the `activeFocus` as intended (e.g., by calling `forceActiveFocus()` right after `activeFocus` of the other item changes to `true`).
