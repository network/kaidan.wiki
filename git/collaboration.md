## Merge Requests (MRs)

You are entirely free when it is about managing your own MR branch. However,
when you're collaborating with other people the following advices may be
helpful.

Incorporate changes from other people working on the MR branch with:

```shell
git checkout ${MR branch name}
git commit --message="Change something"

git fetch ${MR remote name}
git rebase --rebase-merges ${MR remote name}/${MR branch name}

git push ${MR remote name} ${MR branch name}
```

![Commit graph after fetching remote changes](fetched_remote_changes.svg "Commit graph after fetching remote changes")
![Commit graph after rebasing remote changes](rebased_remote_changes.svg "Commit graph after rebasing remote changes")

This way, the changes are included as linear history and merge commits (e.g.
from the `master` branch) are not lost. A deeper [explanation is given by Derek
Gourlay], just note that the option has been renamed from `--preserve-merges` to
`rebase-merges` in the meantime.

Incorporate the current state of `master` with:

```shell
git checkout ${MR branch name}
git commit --message="Change something"

git checkout master
git fetch ${master remote name}
git rebase --rebase-merges ${master remote name}/master

git checkout ${MR branch name}
git merge master

git push ${MR remote name} ${MR branch name}
```

![Commit graph after fetching master changes](fetched_master_changes.svg "Commit graph after fetching master changes")
![Commit graph after merging master changes](merged_master_changes.svg "Commit graph after merging master changes")

With merging instead of rebasing of other branches, history is not rewritten and
forced pushing and resetting is not necessary. Still, the history will be
linearised correctly in the final merge of the MR branch into master.


[explanation is given by Derek Gourlay]:
https://derekgourlay.com/blog/git-when-to-merge-vs-when-to-rebase
