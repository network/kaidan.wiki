# Optimizing Graphics

[SVG](https://en.wikipedia.org/wiki/Scalable_Vector_Graphics) or [PNG](https://en.wikipedia.org/wiki/Portable_Network_Graphics) images can contain parts which are removable or optimizable.
Such an optimization may lead to a reduced file size.
That has an impact on Kaidan's repository and package size.
Additionally, it leads to a smoother user interface because a graphic with a small file size can be loaded and adjusted faster than a graphic with a bigger file size.

## Scalable Vector Graphics (SVG)

[SVGO](https://openbase.io/js/svgo) can be used to optimize SVG images before contributing them.
For an easy usage of SVGO, [SVGO's Missing GUI (SVGOMG)](https://jakearchibald.github.io/svgomg/) is recommended.

### Usage Hints for SVGOMG

Because SVGOMG has many options, the following hints should help with its usage after opening the graphic via the the menu entry *Open SVG* on the left hand side.

* The button of the *Precision* bar should be moved as far as possible to the left as long as the graphic looks the same.
* The option *Remove raster images* should always be used since the graphic should not contain a raster image.
* Options starting with *Round/rewrite* should be used if possible but with caution since elements can look differently after rounding some values.
* The options *Merge paths* and *Clean IDs* can have unwanted consequences.
* The options *Remove xmlns* and *Replace duplicate elements with links* are not recommended.
* If the option *Remove editor data* leads to an undesirable appearance, the graphic should be saved as a normal SVG image via the SVG editing software (to remove all editor specific elements) and opened in SVGOMG again.

## Portable Network Graphics (PNG)

After making sure that [Trimage](https://trimage.org) is installed, you can use our [tool to optimize a PNG image](https://invent.kde.org/network/kaidan/-/blob/master/utils/optimize-png.sh) (Installation on Debian-based systems: `sudo apt install trimage`).
