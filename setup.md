# Basic Setup

This guide explains how to create an account, set up a fork and local repository and contribute to Kaidan.
It is targeted at people who want to start contributing to Kaidan quickly.
Knowledge of KDE Identity, GitLab or Git is not needed.

For further details, have a look at [KDE's guide](https://community.kde.org/Get_Involved), [GitLab's user documentation](https://invent.kde.org/help/user/index) or the [Pro Git book](https://git-scm.com/book).

## Create an Account

1. [Register a KDE Identity account](https://identity.kde.org/index.php?r=registration/index) for contributing to [KDE's GitLab instance](https://invent.kde.org) used by Kaidan.
1. [Log into the GitLab instance](https://invent.kde.org/users/sign_in) with the credentials of your KDE Identity account.

## Set Up Your Fork

For contributing to Kaidan as a public project, a [workflow involving forking](https://git-scm.com/book/en/v2/Distributed-Git-Contributing-to-a-Project#_public_project) is used.

1. Go to [Kaidan's main repository](https://invent.kde.org/network/kaidan).
1. Click on *Fork* to create a connected copy of Kaidan's main repository.
	That fork is your own remote repository where you will upload (in Git terminology called *push*) a new recorded state (called *commit*) of your local repository after changing it.
	The address of your remote repository will be `https://invent.kde.org/<username>/kaidan.git` where `<username>` is your KDE Identity account username (e.g., `https://invent.kde.org/alice/kaidan.git` for the username `alice`).
	That username is also used as your username for contributing.

## Set Up Your Local Repository

1. Install [Git](https://git-scm.com) (For Debian-based systems: `sudo apt install git`).
1. Go to the directory where your local repository (your clone of Kaidan's main repository) should be created:
	```bash
	cd <directory>
	```
	Example:
	```bash
	cd /home/alice/open-source-projects
	```
1. Clone Kaidan's main repository (being the main remote repository with the name *origin*) into the current directory:
	```bash
	git clone https://invent.kde.org/network/kaidan.git
	```
1. Go to your created local repository:
	```bash
	cd kaidan
	```
1. Set your real name for creating commits:
	```bash
	git config user.name "<FirstName> <LastName>"
	```
	Example:
	```bash
	git config user.name "Alice Green"
	```
1. Set your email address for creating commits:
	```bash
	git config user.email <email-address>
	```
	Example:
	```bash
	git config user.email alice.green@example.org
	```
1. Add your remote repository to be used for pushing commits:
	```bash
	git remote add <username> https://invent.kde.org/<username>/kaidan.git
	```
	Example:
	```bash
	git remote add alice https://invent.kde.org/alice/kaidan.git
	```
1. Set the default remote repository for pushing commits:
	```bash
	git config remote.pushDefault <username>
	```
	Example:
	```bash
	git config remote.pushDefault alice
	```
1. Fetch the most up-to-date versions of all remote repositories (those are currently the main remote repository `https://invent.kde.org/network/kaidan.git` and your own forked remote repository `https://invent.kde.org/<username>/kaidan.git`):
	```bash
	git fetch --all
	```

## Set Up Kaidan in IDE *QtCreator*

**[Build Kaidan](https://invent.kde.org/network/kaidan/-/wikis/Home#building-kaidan-from-sources) before continuing!**

### Install Qt Creator and Documentation

QtCreator is an IDE for Qt/QML applications.
Install it and the documentation for Qt (including its QML library *QtQuick*) to view the documentation within QtCreator (For Debian-based systems: `sudo apt install qtcreator qt6-*-doc`).
Afterwards, you can open the documentation of a specific item (such as a class or method) by selecting it and pressing the key *F1*.

If no doucmentation is shown, you maybe need to configure it in QtCreator:

1. Run QtCreator
1. Top bar -> *Edit* -> *Kits* -> *Qt versions* -> *Add…* -> Select `/usr/lib/qt6/bin/qmake6`

If only documentation for Qt 5 is shown, you can remove the settings for that version:

1. Run QtCreator
1. Top bar -> *Edit* -> *Kits* -> *Qt versions* -> Select entry with *Qt 5* -> *Remove*

### Open Kaidan

1. Run QtCreator
1. *Open project*
1. Navigate to project directory *kaidan*
1. Choose *CMakeLists.txt*
1. Left sidebar -> *Projects*
1. *Import Existing Build...*
1. Choose *kaidan/build*

## Contribute Changes

1. Read the [guideline for contributing to Kaidan](https://invent.kde.org/network/kaidan/-/blob/master/CONTRIBUTING.md).
1. Create a branch from Kaidan's main development branch *master* and switch to it for modifying files:
	```bash
	git checkout -b <branch-name> master
	```
	Example:
	```bash
	git checkout -b feature/message-search master
	```
1. Modify the files related to your commit and save them.
1. Mark (called *stage*) the current state of the modified files for storing as a commit:
	```bash
	git add <files>
	```
	Example:
	```bash
	git add CMakeLists.txt README.md
	```
1. Create a commit containing the staged state of the modified files with a short commit message starting with an a upper case letter and using the present tense and imperative:
	```bash
	git commit -m "<commit message>"
	```
	Example:
	```bash
	git commit -m "Add dependencies for notifications"
	```
1. Push the commit to your own remote repository:
	```
	git push
	```
1. Open a merge request (MR) for submitting your changes to Kaidan's main repository:
	1. Go to the overview of your branches on your remote repository `https://invent.kde.org/<username>/kaidan/-/branches` (Example: `https://invent.kde.org/alice/kaidan/-/branches`).
	1. Click on *Merge request* in the row of your branch containing the commit you want to submit (Example: row of *feature/message-search*).
	1. Select *Delete source branch when merge request is accepted.* so that the branch on your remote repository will be deleted when your MR is accepted.
	1. Select *Allow commits from members who can merge to the target branch.* so that the maintainers of Kaidan's main repository can apply necessary changes before merging your branch into the *master* branch.
	1. Click on *Submit merge request*.
