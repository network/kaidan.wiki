This guide describes how to install and update Kaidan's latest development version (nightly) on Linux by using its Flatpak repository.

## Install Flatpak

[Flatpak](https://flatpak.org) needs to be [installed](https://flatpak.org/setup/) for all further steps.

Here is an example for Debian-based systems:

```bash
sudo apt install flatpak
```

## Add the Flathub Repository

Kaidan uses KDE's Flatpak runtime which is in the Flathub repository.
That repository must be added:

```bash
flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
```

## Add Kaidan's Nightly Repository

Kaidan has an own Flatpak repository for nightly builds.
That repository must be added:

```bash
flatpak remote-add --if-not-exists kaidan-nightly https://cdn.kde.org/flatpak/kaidan-nightly/kaidan-nightly.flatpakrepo
```

## Install Kaidan

You can install Kaidan now:

```bash
flatpak install kaidan-nightly im.kaidan.kaidan
```

Once Kaidan is installed, you can start like any other application (e.g., by clicking on its application icon).
Alternatively, you can start it by running the following command:

```bash
flatpak run im.kaidan.kaidan
```

## Update Kaidan

To get the latest version of Kaidan, you can update it:

```bash
flatpak update im.kaidan.kaidan
```

## Configuration

Kaidan's configuration file is `~/.var/app/im.kaidan.kaidan/config/kaidan/kaidan.conf`.

## Database

Kaidan's database file is `~/.var/app/im.kaidan.kaidan/data/kaidan/kaidan.sqlite3`.
